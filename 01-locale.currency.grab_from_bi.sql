drop procedure if exists `locale.currency.grab_from_bi`;
delimiter |

create procedure `locale.currency.grab_from_bi`()
comment 'Purpose :
grab the current currency exchange to RUPIAH, according to Kurs Transaksi Bank Indonesia
Source URL : http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~: coded by RQ | Tuesday, 08 July 2014 01:49:18 WIB :~'
not deterministic contains sql
begin
  declare v_rowcount tinyint unsigned default 0;
  declare v_loop tinyint unsigned default 1;

  set @___url = "http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx";
  set @___page_source = replace(replace(http_get(@___url), '\t' ,''),'  ',' ');
  set @___crop_coordinate_start = locate('ctl00_PlaceHolderMain_biWebKursTransaksiBI_GridView1',@___page_source)+234;
  set @___crop_coordinate_end   = locate('/table',@___page_source,@___crop_coordinate_start)-1;
  set @___cropped_source = substring(@___page_source from @___crop_coordinate_start for (@___crop_coordinate_end - @___crop_coordinate_start));
  set @___page_source = trim(@___cropped_source);
  set @___page_source = replace(replace(replace(replace(@___page_source, '\n' ,''),'\t',''),'\r',''),'  ','');
  set @___page_source = replace(@___page_source,'</td><td class="alignRight">','##');
  set @___page_source = replace(@___page_source,'</td><td style="text-align:right;">','##');
  set @___page_source = replace(@___page_source,'</td><td><input type="image" name="ctl00$PlaceHolderMain$biWebKursTransaksiBI$GridView1$ctl','<|');
  set @___page_source = replace(@___page_source,'$btnGraphImage" id="ctl00_PlaceHolderMain_biWebKursTransaksiBI_GridView1_ctl','');
  set @___page_source = replace(@___page_source,'_btnGraphImage" src="/_biweb/css/images/graph.gif" alt="graph" onclick="SetSource(this.id);" border="0" /></td></tr>','|>');
  set @___page_source = replace(@___page_source,'<tr><td>','');
  set @___page_source = left(@___page_source, length(@___page_source)-8);
  set @___page_source = trim(replace(@___page_source," ",""));
  
  repeat 
    set @___crop1 = locate('<|',@___page_source);
    set @___crop2 = locate('|>',@___page_source,@___crop1)+2;
    if (@___crop2 - @___crop1) > 4 then 
      set @___idx = mid(@___page_source,@___crop1,@___crop2 - @___crop1);
      set @___page_source = replace(@___page_source,@___idx,"|");
    end if;
  until @___crop1 = 0 end repeat;
  
  set v_rowcount = `string.occurances`(@___page_source,'|') + 1;

  drop temporary table if exists `temp_currency`;
  create temporary table `temp_currency` (
      code3 varchar(3) not null primary key,
      rate_sell decimal(26,2),
      rate_middle decimal(26,2),
      rate_buy decimal(26,2)
  ) engine=memory;

  set @___row = null;
  while v_loop <= v_rowcount do 
    set @___row = `string.explode.index`(@___page_source,'|',v_loop);
    set @___code3 = `string.explode.index`(@___row,'##',1);
    set @___rs = replace(`string.explode.index`(@___row,'##',3),',','');
    set @___rb = replace(`string.explode.index`(@___row,'##',4),',','');

    insert `temp_currency` set code3 = @___code3, rate_sell = @___rs, rate_middle = (@___rs + @___rb) / 2, rate_buy = @___rb;

    set @___code3 = null;
    set @___rs = null;
    set @___rb = null;
    set @___row = null;
    set v_loop = v_loop + 1;
  end while;

  select * from `temp_currency`;

  set @___page_source          = null;
  set @__crop_coordinate_start = null;
  set @__crop_coordinate_end   = null;
  set @___cropped_source       = null;
  drop temporary table if exists `temp_currency`;
end |
delimiter ; /* ------------------------------------------------------------------------------------------------------------------------ */
