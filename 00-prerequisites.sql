drop function if exists `string.occurances`;
delimiter ;;
create function  `string.occurances`(kalimat mediumtext, kata text) returns  int(11)
    deterministic
begin

  declare i int;
  declare occurences int default 0;

  set i = locate(kata, kalimat);

  while i > 0
  do
  set occurences = occurences + 1;
  set i = locate(kata, kalimat, i + 1);
  end while;

  return (occurences);

end
;;
delimiter ;

drop function if exists `string.explode.index`;
delimiter ;;
create function  `string.explode.index`(input text,  v_separator varchar(10), col int) returns  varchar(255)
    deterministic
begin
  declare cur_position int default 1 ;
  declare remainder text;
  declare cur_string varchar(1000);
  declare result_string varchar(1000);
  declare delimiter_length tinyint unsigned;
  declare loop_count tinyint unsigned;
  set loop_count=0;
  set remainder = input;
  set delimiter_length = char_length(v_separator);
  while char_length(remainder) > 0 and cur_position > 0 and loop_count<col do
    set cur_position = instr(remainder, v_separator);
    if cur_position = 0 then
      set cur_string = remainder;
    else
      set cur_string = left(remainder, cur_position - 1);
    end if;
    set result_string=cur_string;
    set remainder = substring(remainder, cur_position + delimiter_length);
    set loop_count=loop_count+1;
  end while;
  return trim(both ' ' from result_string);
end
;;
delimiter ;
