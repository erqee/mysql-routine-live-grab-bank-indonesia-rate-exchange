## Live Grabbing Rate Exchange from Bank Indonesia

Source URL : http://www.bi.go.id/id/moneter/informasi-kurs/transaksi-bi/Default.aspx

### Installation

```bash

shell> mysql -uusername -ppassword database_name < 00-prerequisites.sql
shell> mysql -uusername -ppassword database_name < 01-locale.currency.grab_from_bi.sql

```

### Usage Example

```bash 

mysql> call `locale.currency.grab_from_bi`();
+-------+-----------+-------------+----------+
| code3 | rate_sell | rate_middle | rate_buy |
+-------+-----------+-------------+----------+
| AUD   |  11085.49 |    11027.93 | 10970.37 |
| BND   |   9500.36 |     9452.67 |  9404.97 |
| CAD   |  11113.61 |    11055.17 | 10996.72 |
| CHF   |  13231.32 |    13164.69 | 13098.06 |
| CNY   |   1921.24 |     1911.68 |  1902.11 |
| DKK   |   2158.65 |     2147.51 |  2136.36 |
| EUR   |  16095.16 |    16013.83 | 15932.49 |
| GBP   |  20312.34 |    20210.59 | 20108.83 |
| HKD   |   1528.52 |     1520.85 |  1513.17 |
| JPY   |  11597.81 |    11538.92 | 11480.03 |
| KRW   |     11.73 |       11.67 |    11.61 |
| KWD   |  42007.09 |    41775.77 | 41544.46 |
| MYR   |   3716.98 |     3697.31 |  3677.64 |
| NOK   |   1913.11 |     1903.43 |  1893.75 |
| NZD   |  10335.64 |    10280.64 | 10225.64 |
| PGK   |   4972.95 |     4826.21 |  4679.47 |
| PHP   |    272.57 |      271.16 |   269.76 |
| SAR   |   3158.60 |     3142.83 |  3127.05 |
| SEK   |   1729.29 |     1720.12 |  1710.94 |
| SGD   |   9500.36 |     9452.67 |  9404.97 |
| THB   |    365.84 |      363.91 |   361.98 |
| USD   |  11846.00 |    11787.00 | 11728.00 |
+-------+-----------+-------------+----------+
[Finished in 2.0s]

```
